#!/usr/bin/env node
const io = require('socket.io-client')
var options = require('minimist')(process.argv.slice(2));

const DOMAIN = "http://localhost:3003/health-card"
const TRANSACTION_ID = options['t']
const authorization = options['a']

if (!authorization) {
    console.log("No authorization payload provided")
    console.log('Usage Example: npm run connect -- t=transactionId a="Bearer token" ')
}

const socket = io(DOMAIN, { 
    path: "/socket/",
    query: {
        transactionId: `${TRANSACTION_ID}`,
        tenant: 'docmorris'
    },
    extraHeaders: {
        authorization
    }
})

console.log("Socket: Attempting to connect to ", DOMAIN)

socket.on("connect", () => {
    console.log("Socket: Connected...")
})

socket.on("disconnect", (reason) => {
    console.log("Disconnected: ", reason)
})

socket.on(`${TRANSACTION_ID}`, (message) => {
    console.log(`Client Received  message on ${TRANSACTION_ID}: `, message)
})

socket.emit("broker", { data: "Hey Broker"})

setInterval(() => {
    socket.emit("broker", { data: "SEND_ME_SOMETHING" })
}, 3000)